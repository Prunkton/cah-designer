package de.cm.core;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.stream.Stream;

import de.cm.core.model.Card;

public class IO {
	
	private final static String	PLACEHOLDER		   = "Your custom card idea goes here in this box __________.";
	private final static String	PLACEHOLDER_BOX	   = "Basic";
	private static String		svgBlack;
	private static String		svgWhite;
	private static Set<Card>	cardSet			   = new HashSet<>();
	private static Set<Path>	pathSet			   = new HashSet<>();
	private final static String	SEPERATOR_TAB	   = "\t";
	private final static String	SEPARATOR_BOXNAME  = "_";
	private final static String	INDICATOR_BLACK_I  = "__";
	private final static String	INDICATOR_BLACK_II = "?";
	private final static String	FILE_EXT_BLACK	   = "Black";
	private final static String	FILE_EXT_WHITE	   = "White";
	private String				outputFolder	   = "";
	
	private static String boxName;
	
	/*
	 * Read the prototype svg files of the black and white cards into two
	 * strings. we will use them later to build our own cards.
	 */
	public boolean loadSVG(String dir) {
		try {
			svgBlack = new String(Files.readAllBytes(Paths.get(dir + "\\cah-blanks-black.svg")));
			svgWhite = new String(Files.readAllBytes(Paths.get(dir + "\\cah-blanks-white.svg")));
		} catch (IOException e) {
			return false;
		}
		
		if (svgBlack.length() > 0 && svgWhite.length() > 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * Initialise card strings
	 */
	public boolean initCardFiles(String dir) {
		
		try {
// URL mainURL = Main.class.getResource("Main.class");
// Path path =
// Paths.get(mainURL.toURI()).getParent().getParent().getParent().getParent();
// Path folder = Paths.get(path + "\\data\\Cards");
			Path folder = Paths.get(dir);
			
			Files.walk(folder).forEach(filePath -> {
				if (Files.isRegularFile(filePath)) {
					pathSet.add(filePath);
				}
			});
		} catch (/* URISyntaxException | */IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (pathSet.size() > 0) return true;
		return false;
	}
	
	/**
	 * load the card textes the file container must be a .csv-file, also the
	 * file name must include "white" or "black"
	 */
	public boolean loadCards(String dir) {
		pathSet.parallelStream().filter(path -> !path.endsWith(".csv")).forEach(path -> {
			cardSet.addAll(loadCSV(path, path.getFileName().toString().contains("white")));
		});
		if (cardSet.size() > 0) return true;
		return false;
	}
	
	public Set<Card> loadCSV(Path path, boolean isWhite) {
		
		Set<Card> set = new HashSet<>();
		boxName = path.getFileName().toString();
		if (boxName.contains(SEPARATOR_BOXNAME)) boxName = boxName.substring(0, boxName.indexOf(SEPARATOR_BOXNAME));
		try {
			Stream<String> stream = Files.lines(path, Charset.defaultCharset());
			stream.forEach(s -> set.addAll(parseLines(isWhite, s, boxName)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return set;
	}
	
	private Set<Card> parseLines(boolean isWhite, String s, final String boxName) {
		Set<Card> set = new HashSet<>();
		String sub = s.trim();
		int end = 0;
		while (!sub.isEmpty()) {
			end = sub.contains(SEPERATOR_TAB) ? sub.indexOf(SEPERATOR_TAB) : sub.length();
			if (end < 1) break;
			String txt = ((String) sub.subSequence(0, end)).trim();
			txt = txt.replaceAll("\"\"", "\"");
			if (txt.contains("All of this blood")) System.out.println(txt);
			Card c = new Card(isWhite, txt, boxName);
			set.add(c);
			sub = sub.substring(end).trim();
		}
		return set;
	}
	
	/**
	 * the final sgd cards can be opened and printed with the program "Inkscape"
	 */
	public void createSVG() {
		Set<String> fileContentSetBlack = new HashSet<>();
		Set<String> fileContentSetWhite = new HashSet<>();
		String fileContentBlack = svgBlack;
		String fileContentWhite = svgWhite;
		for (Card card : cardSet) {
			if (card.isWhite()) {
				if (!fileContentWhite.contains(PLACEHOLDER)) {
					fileContentSetWhite.add(fileContentWhite);
					fileContentWhite = svgWhite;
				}
				fileContentWhite = fileContentWhite.replaceFirst(PLACEHOLDER, Matcher.quoteReplacement(card.getText()));
				fileContentWhite = fileContentWhite.replaceFirst(PLACEHOLDER_BOX,
						Matcher.quoteReplacement(card.getBoxName()));
			} else {
				if (!fileContentBlack.contains(PLACEHOLDER)) {
					fileContentSetBlack.add(fileContentBlack);
					fileContentBlack = svgBlack;
				}
				fileContentBlack = fileContentBlack.replaceFirst(PLACEHOLDER, Matcher.quoteReplacement(card.getText()));
				fileContentBlack = fileContentBlack.replaceFirst(PLACEHOLDER_BOX,
						Matcher.quoteReplacement(card.getBoxName()));
			}
			System.out.println(card.getText());
		}
		fileContentSetBlack.add(fileContentBlack);
		fileContentSetWhite.add(fileContentWhite);
		exportSVG(false, fileContentSetBlack);
		exportSVG(true, fileContentSetWhite);
	}
	
	private void exportSVG(boolean isWhite, Set<String> svgSet) {
		try {
			int i = 0;
			for (String txt : svgSet) {
				String filename = (outputFolder + "\\" + (isWhite ? FILE_EXT_WHITE : FILE_EXT_BLACK)) + "_" + i
						+ ".svg";
				PrintWriter writer = new PrintWriter(filename, "UTF-8");
				writer.write(txt);
				writer.close();
				i++;
			}
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
// public void loadODT() {
// // load passenger data document
// SpreadsheetDocument data;
// try {
// URL mainURL = Main.class.getResource("Main.class");
//
// File f =
// Paths.get(mainURL.toURI()).toFile().getParentFile().getParentFile().getParentFile();
// data = SpreadsheetDocument.loadDocument(f.getParent() + "\\cah-white.ods");
// List<Table> tableList = data.getTableList();
// for (Table table : tableList) {
// if (false) return;// TODO: check is empty
//
// for (int i = 1; i < table.getRowCount(); i++) {
// Row row = table.getRowByIndex(i);
// for (int j = 0; j < table.getColumnCount(); j++) {
// String content = row.getCellByIndex(j).getDisplayText();
// System.out.println(content);
// }
// }
// }
// } catch (Exception e) {
// // TODO Auto-generated catch block
// e.printStackTrace();
// }
// }
	
	public boolean setOutputFolder(String dir) {
		outputFolder = dir;
		if (outputFolder.length() > 0) return true;
		return false;
	}
}
