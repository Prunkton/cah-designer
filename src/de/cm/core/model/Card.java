package de.cm.core.model;

public class Card {
	private boolean	isWhite;
	private String	txt;
	private String	boxName;
	
	public Card(boolean isWhite, String txt, String boxName) {
		this.boxName = boxName;
		this.isWhite = isWhite;
		this.txt = txt;
		
	}
	
	@Override
	public String toString() {
		return txt;
	}
	
	public String getText() {
		return txt;
	}
	
	public boolean isWhite() {
		return isWhite;
	}
	
	public String getBoxName() {
		return boxName;
	}
	
}
