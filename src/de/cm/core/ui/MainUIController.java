package de.cm.core.ui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import de.cm.core.IO;

public class MainUIController implements Initializable {
	
	private IO				 io;
	private DirectoryChooser dc	= new DirectoryChooser();
	private File			 f;
	private String			 fileFolder;
	private String			 cardFolder;
	private String			 outputFolder;
	
	@FXML
	private GridPane root;
	
	@FXML
	private Label labIndicatorText;
	
	@FXML
	private Label labIndicatorCards;
	
	@FXML
	private TextField tfOutputFolder;
	
	@FXML
	private TextField tfFileFolder;
	
	@FXML
	private TextField tfCardFolder;
	
	@FXML
	private Label labIndicatorOutput;
	
	@FXML
	void actionChoosText(ActionEvent event) {
		fileFolder = chooseFolder();
		tfFileFolder.setText(fileFolder);
		setIndicator(labIndicatorText, (io.initCardFiles(fileFolder) && io.loadCards(fileFolder)));
	}
	
	@FXML
	void actionChoosCards(ActionEvent event) {
		cardFolder = chooseFolder();
		tfCardFolder.setText(cardFolder);
		setIndicator(labIndicatorCards, (io.loadSVG(cardFolder)));
	}
	
	@FXML
	void actionChoosOutput(ActionEvent event) {
		tfOutputFolder.setText(chooseFolder());
		outputFolder = tfOutputFolder.getText();
		setIndicator(labIndicatorOutput, io.setOutputFolder(outputFolder));
	}
	
	@FXML
	void actionCreate(ActionEvent event) {
		if (!fileFolder.equals("") && !cardFolder.equals("") && !outputFolder.equals("")) {
			io.createSVG();
		} else {
			// error
		}
		
// io.loadSVG();
// io.initCardFiles();
// io.loadCards();
// io.createSVG();
	}
	
	private String chooseFolder() {
		f = dc.showDialog(root.getScene().getWindow());
		if (f != null && !f.getAbsolutePath().equals("")) return f.getAbsolutePath();
		return "";
	}
	
	private void setIndicator(Label lab, boolean bool) {
		if (bool) {
			lab.setText(" OK");
			lab.setStyle("-fx-text-fill: green; -fx-font-size: 14;");
		} else {
			lab.setText(" failed");
			lab.setStyle("-fx-text-fill: red; -fx-font-size: 14;");
		}
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		io = new IO();
	}
	
}
