package de.cm.core.ui;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application {
	
	/**
	 * CAH_Designer starts here
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Application.launch(Main.class, (java.lang.String[]) null);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			
			GridPane root = (GridPane) FXMLLoader.load(getClass().getResource("MainUI.fxml"));
			
			Scene scene = new Scene(root);
			
			primaryStage.setScene(scene);
			primaryStage.setTitle("CAH Designer");
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
				@Override
				public void handle(WindowEvent arg0) {
					System.exit(0);
				}
			});
			primaryStage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
