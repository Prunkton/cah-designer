### CAH_Designer

This is a minimal program to create own cards for the cardgame "Cards against Humanity".
Card text lists and the card designs can be found on http://mdsc.info/dropbox/cah/
The card designs will be stored in the folder "data". The file type is .svg, it can be opened e.g. with Inkscape.
The cards text must be inpupt by the user under data/Cards. The filetype must be .csv and contain the string "white" or "black" to indicate the card type (e.g.: whiteCards.csv).
The final cards will also be exported as .svg files and can be printed as well with Inkscape.

The official game can be found under https://cardsagainsthumanity.com/

Cards Against Humanity is a trademark of Cards Against Humanity, LLC. Cards Against Humanity is distributed under a Creative Commons License license. Cards Against Humanity, LLC does not endorse me, this website, or any of the files here in any way. I did not create the game, nor am I attempting to make any money by selling it. I have provided these documents free of charge for personal use and entertainment purposes.